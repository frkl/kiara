# -*- coding: utf-8 -*-

#  Copyright (c) 2021, University of Luxembourg / DHARPA project
#  Copyright (c) 2021, Markus Binsteiner
#
#  Mozilla Public License, version 2.0 (see LICENSE or https://www.mozilla.org/en-US/MPL/2.0/)


# def test_operation_type_list(kiara: Kiara):
#
#     assert "calculate_hash" in kiara.operation_mgmt.operation_types.keys()
#
#     ch_ops = kiara.operation_mgmt.get_operations("calculate_hash")
#     ch_tm = ch_ops.get_type_metadata()
#     assert ch_tm.type_name == "calculate_hash"
